#!/usr/bin/env python3

import argparse
import logging
import os
import sys

import mkhost.cfg
import mkhost.common
import mkhost.dns_log
import mkhost.dovecot
import mkhost.letsencrypt
import mkhost.opendkim
import mkhost.postfix
import mkhost.postsrsd
import mkhost.unix

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Re-configures this machine according to the hardcoded configuration (mkhost/cfg.py). Edit that configuration file first.',
        add_help=True, allow_abbrev=False, epilog="""This program comes with ABSOLUTELY NO WARRANTY.""")

    parser.add_argument("--batch",
                        required=False,
                        action="store_true",
                        default=False,
                        help="batch mode (non-interactive)")

    parser.add_argument("--dry-run",
                        required=False,
                        action="store_true",
                        default=False,
                        help="dry run (no change)")

    parser.add_argument("--show-config",
                        required=False,
                        action="store_true",
                        default=False,
                        help="print the configuration and exit")

    parser.add_argument("--generate-dkim-keys",
                        required=False,
                        choices=['all', 'missing', 'none'],
                        default='missing',
                        help="which DKIM keys to generate; by default only the missing ones will be generated")

    parser.add_argument("--verbose",
                        required=False,
                        action="store_true",
                        default=False,
                        help="verbose processing")

    parser.add_argument("--version",
                        required=False,
                        action="store_true",
                        default=False,
                        help="print version number and exit")

    # Parse command line arguments
    args = parser.parse_args()

    # Setup logging
    log_format = '[{asctime}] {levelname:8} {threadName:<14} {message}'
    logging.basicConfig(stream=sys.stderr, level=(logging.DEBUG if args.verbose else logging.INFO), format=log_format, style='{')

    if args.version:
        mkhost.cfg_parser.show_version()
        sys.exit()

    # Setup global variables
    mkhost.common.set_generate_dkim_keys(args.generate_dkim_keys)
    mkhost.common.set_verbose(args.verbose)
    mkhost.common.set_dry_run(args.dry_run)
    mkhost.common.set_non_interactive(args.batch)

    if args.show_config:
        mkhost.cfg_parser.show_config()
        mkhost.cfg_parser.validate()
        sys.exit()

    # validate config
    mkhost.cfg_parser.validate()

    # Destructively re-configure the machine
    logging.info("Updating system packages...")
    mkhost.unix.update_pkgs()
    logging.info("Setting up Let's Encrypt...")
    mkhost.letsencrypt.install()

    if mkhost.cfg.ENABLE_SRS:
        logging.info("Setting up SRS...")
        mkhost.postsrsd.install()

    if mkhost.cfg.ENABLE_DKIM:
        logging.info("Setting up OpenDKIM...")
        mkhost.opendkim.install()

    logging.info("Setting up Dovecot...")
    mkhost.dovecot.install()
    logging.info("Setting up Postfix...")
    mkhost.postfix.install()

    mkhost.dns_log.generate_records()
    mkhost.dns_log.print_log()
    logging.info("mkhost: setup complete. Restarting services...")

    if mkhost.cfg.ENABLE_SRS:
        mkhost.postsrsd.restart()
    if mkhost.cfg.ENABLE_DKIM:
        mkhost.opendkim.restart()
    mkhost.dovecot.restart()
    mkhost.postfix.restart()
