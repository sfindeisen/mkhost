# Create your own mail server in just a couple of lines

## Declarative, idempotent mail server configuration script

`mkhost` makes it easy to setup your own simple mail server, complete with: unlimited number of domains, unlimited number of mailboxes, unlimited number of aliases (both static and dynamic using `+`), unlimited mail forwarding to 3rd party addresses, [DKIM](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail) support and TLS/SSL support (including X.509 certificate management). Typical use cases include: small- or medium-sized enterprise, family or household, cheap mail (self-)hosting of multiple domains, webapp prototyping etc.

Everything you need is a [single configuration file](mkhost/cfg.py). If you ever need to make a change later, don't worry: `mkhost` will take care of carefully patching your existing configuration with the latest changes.

## Features

1. TLS/SSL certificates (by [Let's Encrypt](https://letsencrypt.org/))
2. DKIM (by [OpenDKIM](http://www.opendkim.org/))
3. Sender Rewriting Scheme (SRS) (by [postsrsd](https://packages.debian.org/stable/postsrsd))
4. SMTP server ([Postfix](http://www.postfix.org/))
5. IMAP/POP3 server ([Dovecot](https://www.dovecot.org/))
6. batch and interactive modes
7. dry run mode

## Synopsis

```
$ mkhost.py --help
usage: mkhost.py [-h] [--batch] [--dry-run] [--show-config] [--generate-dkim-keys {all,missing,none}] [--verbose] [--version]

Re-configures this machine according to the hardcoded configuration (mkhost/cfg.py). Edit that configuration file first.

optional arguments:
  -h, --help            show this help message and exit
  --batch               batch mode (non-interactive)
  --dry-run             dry run (no change)
  --show-config         print the configuration and exit
  --generate-dkim-keys {all,missing,none}
                        which DKIM keys to generate; by default only the missing ones will be generated
  --verbose             verbose processing
  --version             print version number and exit

This program comes with ABSOLUTELY NO WARRANTY.
```

# Requirements

## Requirements for the target mail host

1. [Debian](https://www.debian.org/) GNU/Linux based operating system (with `apt-get`) (verified to work with: Debian GNU/Linux 10,11)
2. Python 3.x

## Requirements for your local machine

None.

# How to run

1. Edit [configuration file](mkhost/cfg.py) as needed
2. Transfer files to the (remote) target mail host

   Example:

   ```bash
   find . -name '*.git' -prune -o -name '*.pyc' -prune -o -type f -print0
     | tar --null --files-from=- -c
     | ssh my-remote-user@my-remote-host tar --one-top-level=mkhost-repo -xvf - -C /home/my-remote-user/
   ```

3. Login to the (remote) target mail host and execute (as `root`):

   ```
   mkhost.py
   ```

   This will (re-)configure your machine.

# How to setup your DNS records

You need to configure [`MX`](https://en.wikipedia.org/wiki/MX_record), [`SPF`](https://en.wikipedia.org/wiki/Sender_Policy_Framework), [`DKIM`](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail), [`DMARC`](https://en.wikipedia.org/wiki/DMARC) and [`PTR`](https://en.wikipedia.org/wiki/List_of_DNS_record_types#PTR) DNS records manually in your DNS zone(s). `MX` record is mandatory. `SPF`, `DKIM`, `DMARC` and `PTR` records will improve your protection against e-mail spoofing or improve the reputation of your server and are recommended, but not required.

Example:

Let's assume your mail host (`MY_HOST_FULLNAME`) is `mylinux.myhome.me` and your hosted (virtual) mail domain (`MAILBOXES`) is `mycorp.com`. Then you could define the following records in your `mycorp.com` DNS zone:

```
mycorp.com.  3600  IN  MX   10 mylinux.myhome.me.
mycorp.com.  3600  IN  TXT  "v=spf1 +mx -all"
```

Note: `MX`, `SPF`, `DKIM` and `DMARC` records will be generated and printed by the script. You need to copy them to the respective DNS zone(s). You might want to adapt `MX` record priorities (they're all `10` by default), `SPF` qualifiers or mechanisms or `DMARC` policies or alignment options (e.g. easing your `DMARC` policy with `p=quarantine` could be a good idea for the initial rollout). `PTR` record must be configured by your ISP to point back to your mailhost (`MY_HOST_FULLNAME`).

# How to test

Here are some 3rd party services which you can use to verify your installation (use at your own risk):

1. `TLS`/`SSL` check: https://testtls.com/ or https://ssl-tools.net/ ; you can also [test directly using OpenSSL](https://doc.dovecot.org/configuration_manual/dovecot_ssl_configuration/#testing)
2. Mail delivery problems: https://mxtoolbox.com/
3. `SPF` DNS record(s): https://www.dmarcanalyzer.com/spf/checker/
4. Once you have published your `DKIM` DNS record(s), you can use https://appmaildev.com/en/dkim to send an e-mail and have your actual signatures verified.
5. Two other services for `DKIM` testing are mentioned [here](https://wiki.debian.org/opendkim#email_tests).
6. `DMARC` validation: https://app.dmarcanalyzer.com/dns/dmarc_validator or https://dmarcian.com/dmarc-inspector/ .

# Caveats

1. Non-admin users cannot change their passwords

   User authentication is handled by [Dovecot SASL](https://doc.dovecot.org/admin_manual/sasl/). Virtual user passwords are stored encrypted in a [passwd file](https://doc.dovecot.org/configuration_manual/authentication/passwd_file/). This is a minimalistic user management mechanism which does not require a SQL database or LDAP, but we don't know of a generic way for a non-admin user to change anyone's password.

   Virtual user accounts in this context are non-UNIX user accounts which are internal to Postfix/Dovecot. They can be used as recipient/sender addresses or for authentication. You can read more about this concept [here](http://www.postfix.org/VIRTUAL_README.html#virtual_mailbox).

2. Batch mode

   For each virtual user, the password is auto-generated on the first run and printed to the [log](https://docs.python.org/3/library/logging.html), so make sure to take a note of it (and to delete the log file, if any).

3. Postfix without chroot?

   The non-default, non-recommended setup of Postfix running without a chroot (`POSTFIX_CHROOT = False`) has never been tested.

# FAQ

1. Does `mkhost` include any kind of web server, webmail or mailing list manager?

   No. This is just a basic script for installing and configuring the essential things. If you need any extra tools, you can install them from [Debian](https://packages.debian.org/stable/mail/).

# Feedback and contributions

You are welcome! Please get in touch.

# TODO

01. [ ] Postfix `master.cf` customization
02. [x] (WON'T FIX) opendkim: skip new selector generation if a recent one already exists; check public/private key files and DNS records!
03. [x] (WON'T FIX) SSH hardening
04. [x] intermediary certificate (let's encrypt) missing error (some clients)
05. [x] (WON'T FIX) reverse DNS check
06. [ ] DANE support: https://ssl-tools.net/dane
07. [x] generate DNS records for SPF
08. [x] generate DNS records for DMARC
09. [ ] (BEGINNER-FRIENDLY) make virtual user password length configurable
10. [x] generate MX DNS records
11. [ ] get rid of `OPENDKIM_USER` and `OPENDKIM_GROUP` config settings; these should be inferred from OpenDKIM's config file instead; `UserID` is there, and then you can use `mkhost.unix.get_user_info` to get the default group name
12. [ ] Postfix chroot: compare http://www.postfix.org/postconf.5.html#queue_directory with `POSTFIX_CHROOT_PATH`; bail out on mismatch (e.g. in `cfg_parser.validate()`)
13. [ ] (?) generate DNS records for the host domain (`MY_HOST_FULLNAME`/`MY_HOST_DOMAIN`)
14. [ ] check if `postmaster@` e-mail address, as used in `DMARC`, actually exists; make it configurable?...
15. [ ] (?) DKIM-sign internally generated mail, e.g. bounces? check here: http://www.postfix.org/postconf.5.html#internal_mail_filter_classes
16. [ ] (?) generate `PTR` DNS record
17. [ ] (?) generate `A` and `AAAA` DNS records
18. [ ] (?) mailing list support
19. [x] fix Postfix forwarding using SRS, see: https://serverfault.com/questions/896791/postfix-forwarding-spf-issues-sender-rewrite , https://serverfault.com/questions/635293/postfix-as-email-forwarder-to-gmail-spf-problems , https://packages.debian.org/stable/postsrsd ; but see the comments, perhaps we don't want to authorize spam?... this should be a configuration option perhaps.
20. [x] check if `x509@` e-mail address, as used in SSL/TLS certificate, actually exists; make it configurable?... see 21
21. [x] generate reasonable /etc/aliases
22. [ ] upon installing Postfix in interactive mode, pop-up dialogs from apt-get/debconf appear; but then the values are overwritten in the config file anyway; this is confusing.
23. [ ] iptables
24. [ ] (?) ARC: https://lists.mailman3.org/archives/list/mailman-users@mailman3.org/message/KFUJ7M3EUFR37JICDLM6R52CHKBQMKBL/ ; see also: https://lists.mailman3.org/archives/list/mailman-users@mailman3.org/thread/FS4GUZOFWQNEOPUOT6LWD7TT6P2MXOXS/
25. [ ] `/etc/aliases`: check if all the (system) users exist
26. [ ] check for cyclic references in `MAIL_FORWARDING`
27. [ ] implement support for: http://www.postfix.org/postconf.5.html#smtpd_sender_login_maps and apply it here: http://www.postfix.org/postconf.5.html#smtpd_sender_restrictions (`reject_sender_login_mismatch`)
28. [ ] (?) harden your email server using fail2ban: https://landchad.net/mail/security/
29. [ ] Make `postconf` commands in `dry-run` mode really dry
