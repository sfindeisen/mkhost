import copy
import functools
import logging
import os.path
import re

import mkhost.cfg
import mkhost.cfg_parser
import mkhost.common
import mkhost.file_parser
import mkhost.file_writer
import mkhost.letsencrypt
import mkhost.unix

# TODO simplified /etc/aliases file parsing disregarding continued lines (logical line spanning multiple physical lines) and embedded commas (,)
re_aliases   = re.compile(
    '^([\w.-]+)\s*:\s*([\w"|/. -]+)((?:\s*,\s*[\w"|/. -]+)*)$', re.ASCII)
re_valias    = re.compile(
    '^([^@]+)@([^@]+?)\s+(\S+@\S+)((?:\s*,\s*\S+@\S+)*)$', re.ASCII)
re_vmailbox  = re.compile(
    '^([^@]+)@([^@]+?)\s+(\S+)$', re.ASCII)

def postconf_del(key):
    mkhost.cmd.execute_cmd(["postconf", "-v", "-#", "{}".format(key)])

def postconf_get(key):
    return mkhost.cmd.execute_cmd(["postconf", "-h", "{}".format(key)])[0][0]

def postconf_set(key, value):
    mkhost.cmd.execute_cmd(["postconf", "-v", "-e", "{}={}".format(key,value)])

def postconf_set_multiple(key, values):
    if values:
        postconf_set(key, ' '.join(filter(bool, values)))
    else:
        postconf_del(key)

# Configures Postfix-OpenDKIM integration; see:
#
# https://wiki.debian.org/opendkim#Postfix_integration
# http://www.postfix.org/postconf.5.html#smtpd_milters
# http://www.postfix.org/postconf.5.html#non_smtpd_milters
def postconf_opendkim():
    if mkhost.cfg.ENABLE_DKIM:
        postconf_set('milter_default_action', 'accept')  # see: https://wiki.debian.org/opendkim#Postfix_integration
        postconf_set('smtpd_milters',         'unix:' + os.path.join("/", mkhost.cfg.POSTFIX_OPENDKIM_SOCKET))
        postconf_set('non_smtpd_milters',     '$smtpd_milters')

# Configures Sender Rewriting Scheme (SRS).
def postconf_srs():
    if mkhost.cfg.ENABLE_SRS:
        postconf_set('sender_canonical_maps',       'tcp:localhost:10001')
        postconf_set('sender_canonical_classes',    'envelope_sender')
        postconf_set('recipient_canonical_maps',    'tcp:localhost:10002')
        postconf_set('recipient_canonical_classes', 'envelope_recipient,header_recipient')
    else:
        postconf_del('sender_canonical_maps')
        postconf_del('recipient_canonical_maps')

# Basic Postfix configuration settings using postconf.
def postconf_all():
    postconf_set('alias_database',               'hash:{}'.format(mkhost.cfg.ETC_ALIASES))

    # Restrict local(8) mail delivery to external commands.
    #
    # http://www.postfix.org/postconf.5.html#allow_mail_to_commands
    postconf_set('allow_mail_to_commands',       'alias')

    # Restrict local(8) mail delivery to external files.
    #
    # http://www.postfix.org/postconf.5.html#allow_mail_to_files
    postconf_set('allow_mail_to_files',          'alias')

    # Defer delivery when a mailbox file is not owned by its recipient.
    # (Apparently this only applies to local(8) delivery.)
    #
    # http://www.postfix.org/postconf.5.html#strict_mailbox_ownership
    postconf_set('strict_mailbox_ownership',     'yes')

    postconf_set('biff',                         'no')
    postconf_set('broken_sasl_auth_clients',     'no')
    postconf_set('delay_warning_time',           '4h')
    postconf_set('inet_interfaces',              'all')
    postconf_set('lmtp_sasl_auth_enable',        'no')
    postconf_set('mydomain',                     mkhost.cfg.MY_HOST_DOMAIN)
    postconf_set('myhostname',                   mkhost.cfg_parser.get_host_fullname())
    postconf_del('mynetworks')
    postconf_set('mynetworks_style',             'host')

    # mydestination: http://www.postfix.org/postconf.5.html#mydestination
    postconf_set_multiple('mydestination',
                          ['localhost', 'localhost.$myhostname', '$myhostname']
                          + (['localhost.$mydomain', '$mydomain'] if mkhost.cfg.MY_HOST_DOMAIN_MX else []))

    postconf_set('myorigin',                     '$myhostname')
    postconf_set('recipient_delimiter',          '+')

    postconf_set('smtp_sasl_auth_enable',        'no')
    postconf_set('smtp_tls_loglevel',            '1')
    postconf_set('smtp_tls_security_level',      'may')

    # TODO: reject_rbl_client zen.spamhaus.org ?
    postconf_set('smtpd_recipient_restrictions', 'permit_mynetworks permit_sasl_authenticated reject_unauth_destination')
    postconf_set('smtpd_relay_restrictions',     'permit_mynetworks permit_sasl_authenticated reject_unauth_destination')
    # TODO: make this a cfg setting
    postconf_set('smtpd_sasl_auth_enable',       'yes')
    postconf_set('smtpd_tls_auth_only',          'yes')
    postconf_set('smtpd_tls_cert_file',          mkhost.letsencrypt.fullchain_path())
    postconf_set('smtpd_tls_ciphers',            'high')
    postconf_set('smtpd_tls_key_file',           mkhost.letsencrypt.key_path())
    postconf_set('smtpd_tls_loglevel',           '1')
    postconf_set('smtpd_tls_mandatory_ciphers',  'high')
    postconf_set('smtpd_tls_security_level',     'may')
    postconf_set('smtpd_tls_wrappermode',        'no')
    postconf_set('smtpd_sasl_path',              'private/auth')
    postconf_set('smtpd_sasl_security_options',  'noanonymous noplaintext')
    postconf_set('smtpd_sasl_tls_security_options', 'noanonymous')

    # The SASL plug-in type that the Postfix SMTP server should use for authentication.
    # The available types are listed with the "postconf -a" command.
    # http://www.postfix.org/postconf.5.html#smtpd_sasl_type
    # TODO: check if dovecot is available! error if not.
    postconf_set('smtpd_sasl_type',              'dovecot')

    # With SSLv3 and later, use the Postfix SMTP server's cipher preference order
    # instead of the remote client's cipher preference order.
    # http://www.postfix.org/postconf.5.html#tls_preempt_cipherlist
    postconf_set('tls_preempt_cipherlist',       'yes')

    # Optional pathname of a mailbox file relative to a local(8) user's home directory.
    #
    # http://www.postfix.org/postconf.5.html#home_mailbox
    postconf_set('home_mailbox', 'Maildir/')

    # virtual alias domains
    #
    # http://www.postfix.org/postconf.5.html#virtual_alias_domains
    postconf_set_multiple('virtual_alias_domains', mkhost.cfg_parser.get_virtual_alias_domains())

    # http://www.postfix.org/postconf.5.html#virtual_alias_maps
    postconf_set('virtual_alias_maps', "hash:{}".format(mkhost.cfg.POSTFIX_VIRTUAL_ALIAS_MAP))

    # virtual mailbox base (aka directory where virtual mail is stored)
    #
    # http://www.postfix.org/postconf.5.html#virtual_mailbox_base
    postconf_set('virtual_mailbox_base', mkhost.cfg.VIRTUAL_MAILBOX_BASE)

    # virtual mailbox domains
    #
    # http://www.postfix.org/postconf.5.html#virtual_mailbox_domains
    postconf_set_multiple('virtual_mailbox_domains', mkhost.cfg_parser.get_virtual_mailbox_domains())

    # http://www.postfix.org/postconf.5.html#virtual_mailbox_maps
    postconf_set('virtual_mailbox_maps', "hash:{}".format(mkhost.cfg.POSTFIX_VIRTUAL_MAILBOX_MAP))

    # virtual mail ownership
    (vm_uid, vm_gid) = mkhost.unix.get_user_info(mkhost.cfg.VIRTUAL_MAIL_USER)
    postconf_set('virtual_minimum_uid', vm_uid)
    postconf_set('virtual_uid_maps', "static:{}".format(vm_uid))
    postconf_set('virtual_gid_maps', "static:{}".format(vm_gid))

    # configure OpenDKIM and Sender Rewriting Scheme (SRS)
    postconf_opendkim()
    postconf_srs()

# Given a match object which is assumed to be a match of re_aliases regex in
# ETC_ALIASES file, updates the given mail aliases map (local_aliases)
# accordingly.
#
# This is to be used as callback function with mkhost.file_parser.parse_config_file.
def _parse_etc_aliases_line_match(local_aliases, m):
    sname = m.group(1)         # source
    tval1 = m.group(2)         # 1st target
    tvals = list(filter(bool, map(lambda x: x.strip(), m.group(3).split(','))))
    tvals.insert(0,tval1)

    if (sname in local_aliases):
        mto = mkhost.common.tolist(local_aliases[sname])

        if (len(tvals) == len(mto)) and (set(tvals) == set(mto)):
            logging.debug("[postfix] local alias already exists: {} => {}".format(sname, mto))
            del local_aliases[sname]
            return True
        else:
            logging.info("[postfix] delete local alias: {} => {}".format(sname, tvals))
            return False
    else:
        logging.info("[postfix] delete local alias: {}".format(sname))
        return False

# Generates and writes out /etc/aliases file (mkhost.cfg.ETC_ALIASES).
def write_etc_aliases():
    local_aliases = copy.deepcopy(mkhost.cfg.LOCAL_ALIASES)

    old_lines = mkhost.file_parser.parse_config_file(
        mkhost.cfg.ETC_ALIASES,
        re_aliases,
        functools.partial(_parse_etc_aliases_line_match, local_aliases))

    # create new /etc/aliases file
    with mkhost.file_writer.create_tmp_text_file(old_lines) as f:
        if local_aliases:
            mkhost.file_writer.write_mkhost_header(f)
            for x in local_aliases:
                ys = mkhost.common.tolist(local_aliases[x])
                logging.info("[postfix] create local alias: {} => {}".format(x, ys))
                print("{:<24}: {}".format(x, ", ".join(ys)), file=f)

        # overwrite old /etc/aliases file
        mkhost.common.flush_copy_execute(f, mkhost.cfg.ETC_ALIASES, cmd=["newaliases"])

# Given a match object which is assumed to be a match of re_valias regex in
# POSTFIX_VIRTUAL_ALIAS_MAP file, updates the given mail forwarding map
# (mail_forwarding) accordingly.
#
# This is to be used as callback function with mkhost.file_parser.parse_config_file.
def _parse_valias_map_line_match(mail_forwarding, m):
    suser  = m.group(1)         # source user
    sdom   = m.group(2)         # source domain
    taddr1 = m.group(3)         # 1st target address
    taddrs = list(filter(bool, map(lambda x: x.strip(), m.group(4).split(','))))

    saddr = "{}@{}".format(suser, sdom)     # source address
    taddrs.insert(0,taddr1)

    if (saddr in mail_forwarding):
        mto = mkhost.common.tolist(mail_forwarding[saddr])

        if (len(taddrs) == len(mto)) and (set(taddrs) == set(mto)):
            logging.debug("[postfix] mapping already exists: {} => {}".format(saddr, mto))
            del mail_forwarding[saddr]
            return True
        else:
            logging.info("[postfix] delete mapping: {} => {}".format(saddr, taddrs))
            return False
    else:
        logging.info("[postfix] delete mapping: {}".format(saddr))
        return False

# Generates and writes out virtual alias map file (mkhost.cfg.POSTFIX_VIRTUAL_ALIAS_MAP).
def write_valias_map():
    mail_forwarding = copy.deepcopy(mkhost.cfg.MAIL_FORWARDING)

    old_lines = mkhost.file_parser.parse_config_file(
        mkhost.cfg.POSTFIX_VIRTUAL_ALIAS_MAP,
        re_valias,
        functools.partial(_parse_valias_map_line_match, mail_forwarding))

    # create new virtual alias map file
    with mkhost.file_writer.create_tmp_text_file(old_lines) as f:
        if mail_forwarding:
            mkhost.file_writer.write_mkhost_header(f)
            for x in mail_forwarding:
                ys = mkhost.common.tolist(mail_forwarding[x])
                logging.info("[postfix] create mapping: {} => {}".format(x, ys))
                print("{:<48} {}".format(x, ", ".join(ys)), file=f)

        # overwrite old user db file
        mkhost.common.flush_copy_execute(f, mkhost.cfg.POSTFIX_VIRTUAL_ALIAS_MAP, cmd=["postmap", mkhost.cfg.POSTFIX_VIRTUAL_ALIAS_MAP])

# Given a match object which is assumed to be a match of re_vmailbox regex in
# POSTFIX_VIRTUAL_MAILBOX_MAP file, updates the given mailbox set
# (virtual_mailboxes) accordingly.
#
# This is to be used as callback function with mkhost.file_parser.parse_config_file.
def _parse_vmailbox_map_line_match(virtual_mailboxes, m):
    username = m.group(1)
    domain   = m.group(2)
    path     = m.group(3)

    # TODO: make the 2nd lookup more effective?...
    if (domain in mkhost.cfg.MAILBOXES) and (username in mkhost.cfg.MAILBOXES[domain]):
        logging.debug("[postfix] mailbox already exists: {}@{}".format(username, domain))
        virtual_mailboxes.remove("{}@{}".format(username, domain))
        return True
    else:
        logging.info("[postfix] delete mailbox: {}@{}".format(username, domain))
        return False

# Generates and writes out virtual mailbox map file (mkhost.cfg.POSTFIX_VIRTUAL_MAILBOX_MAP).
def write_vmailbox_map():
    virtual_mailboxes = mkhost.cfg_parser.get_virtual_mailboxes()

    old_lines = mkhost.file_parser.parse_config_file(
        mkhost.cfg.POSTFIX_VIRTUAL_MAILBOX_MAP,
        re_vmailbox,
        functools.partial(_parse_vmailbox_map_line_match, virtual_mailboxes))

    # create new virtual mailbox map file
    with mkhost.file_writer.create_tmp_text_file(old_lines) as f:
        if virtual_mailboxes:
            mkhost.file_writer.write_mkhost_header(f)
            for x in virtual_mailboxes:
                xp = mkhost.common.parse_addr(x)
                logging.info("[postfix] create mailbox: {}@{}".format(xp[0],xp[1]))
                print("{:<48} {}/{}/mail/".format("{}@{}".format(xp[0],xp[1]),xp[1],xp[0]), file=f)

        # overwrite old user db file
        mkhost.common.flush_copy_execute(f, mkhost.cfg.POSTFIX_VIRTUAL_MAILBOX_MAP, cmd=["postmap", mkhost.cfg.POSTFIX_VIRTUAL_MAILBOX_MAP])

# Creates a system user for owning virtual mail files.
def setup_vmail_user():
    mkhost.unix.add_system_user(mkhost.cfg.VIRTUAL_MAIL_USER)

# Creates virtual mail dir(s).
def setup_vmail_dirs():
    if not os.path.isdir(mkhost.cfg.VIRTUAL_MAILBOX_BASE):
        (vm_uid, vm_gid) = mkhost.unix.get_user_info(mkhost.cfg.VIRTUAL_MAIL_USER)
        if not mkhost.common.get_dry_run():
            mkhost.unix.makedir(mkhost.cfg.VIRTUAL_MAILBOX_BASE, vm_uid, vm_gid)

def setup_opendkim_unix():
    if mkhost.cfg.ENABLE_DKIM:
        (opendkim_uid, opendkim_gid) = mkhost.unix.get_user_info(mkhost.cfg.OPENDKIM_USER)

        if not mkhost.common.get_dry_run():
            mkhost.unix.add_user_to_group(mkhost.cfg.POSTFIX_USER, mkhost.cfg.OPENDKIM_GROUP)
            mkhost.unix.makedir(os.path.dirname(mkhost.cfg.OPENDKIM_SOCKET), opendkim_uid, opendkim_gid, exist_ok=True, mode=0o770)

# Installs and configures Postfix.
def install():
    mkhost.unix.install_pkgs(["postfix"])

    if mkhost.cfg.ENABLE_DKIM:
        setup_opendkim_unix()

    setup_vmail_user()
    setup_vmail_dirs()

    postconf_all()

    # Because write_etc_aliases executes newaliases, and both
    # write_valias_map and write_vmailbox_map execute postmap,
    # it's better if basic configuration (postconf_all) has
    # already been done. For example, newaliases relies on
    # alias_database setting.

    write_etc_aliases()
    write_vmailbox_map()
    write_valias_map()

def restart():
    mkhost.unix.restart('postfix')
