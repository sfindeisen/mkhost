import os.path

import mkhost.cfg
import mkhost.cfg_parser
import mkhost.cmd
import mkhost.common
import mkhost.unix

def fullchain_path():
    return os.path.join(
        mkhost.cfg.LETSENCRYPT_HOME, "live", mkhost.cfg_parser.get_host_fullname(), "fullchain.pem")

def key_path():
    return os.path.join(
        mkhost.cfg.LETSENCRYPT_HOME, "live", mkhost.cfg_parser.get_host_fullname(), "privkey.pem")

def _run_certbot(domain, mailaddr):
    mkhost.cmd.execute_cmd(
        ["certbot", "certonly"] + \
        (["--dry-run"] if mkhost.common.get_dry_run() else []) + \
        (["--non-interactive", "--agree-tos"] if mkhost.common.get_non_interactive() else []) + \
        ["--standalone"] + \
        ["--email", "{}".format(mailaddr)] + \
        ["--redirect", "--domain", domain])

# Installs Let's Encrypt's X.509 certificate(s).
def install():
    mkhost.unix.install_pkgs(["certbot"])

    # install certificate for the host (MY_HOST_NAME.MY_HOST_DOMAIN)
    _run_certbot(mkhost.cfg_parser.get_host_fullname(), mkhost.cfg.X509_EMAIL)

    # install certificates for extra virtual domains (EXTRA_VIRTUAL_DOMAINS)
    for domain, mailaddr in mkhost.cfg.EXTRA_VIRTUAL_DOMAINS.items():
        _run_certbot(domain, mailaddr)

    # No need to install certificates for (regular) virtual domains, because they
    # are... well, virtual (mail-only). Their mail is handled by the host
    # (MY_HOST_NAME.MY_HOST_DOMAIN) (based on the DNS MX record).
