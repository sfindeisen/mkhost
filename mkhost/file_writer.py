import contextlib
import logging
import os
import tempfile

import mkhost.common

# Creates, opens for writing, optionally writes the given lines and yields
# a new temporary text file, leaving it open for future writes. The file
# will be automatically deleted after it has been closed.
@contextlib.contextmanager
def create_tmp_text_file(lines=None):
    with tempfile.NamedTemporaryFile(mode="wt", prefix="mkhost-", delete=True) as f:
        logging.debug("[file_writer] temp file: {}".format(f.name))
        if lines:
            print(os.linesep.join(lines), file=f)
        yield f

# Given an open text file, writes the mkhost header to it.
def write_mkhost_header(f):
    print(mkhost.common.mkhost_header(), file=f)
