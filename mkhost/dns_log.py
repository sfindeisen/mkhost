import logging
import os

import mkhost.cfg_parser

# Log of changes to be applied to DNS (externally).
#
# A DNS record is currently implemented as a 3-tuple: (name, type, value).
class DNSLog:
    def __init__(self):
        # A list of DNS records (to be printed at the end).
        self.records = []

    def add_record(self, rec: tuple[str,str,str]):
        self.records.append(rec)

    def __len__(self):
        return len(self.records)

    def __bool__(self):
        return (len(self) >= 1)

    def __str__(self):
        # sort DNS records by inverted domain name 1st, record type 2nd
        recs = sorted(self.records, key=(lambda x: ('.'.join(x[0].split('.')[::-1]), x[1], x[2])))
        return os.linesep.join(map(lambda x: "{: <52} {: <6} {}".format(x[0],x[1],x[2]), recs))

def print_log():
    if mkhost.common.dnslog:
        logging.info("List of DNS changes to apply:{}{}".format(os.linesep, mkhost.common.dnslog))
    else:
        logging.info("No DNS changes to apply")

# Generates MX, SPF and DMARC records for each virtual domain and appends them to the log.
def generate_records():
    for dom in mkhost.cfg_parser.get_all_domains(include_extra=True):
        if dom.endswith('.'):
            dom = dom[:-1]
        ddot = dom + '.'

        mkhost.common.dnslog.add_record((ddot, 'MX',  '10 {}'.format(mkhost.cfg_parser.get_host_fullname())))
        mkhost.common.dnslog.add_record((ddot, 'TXT', 'v=spf1 +mx -all'))
        # quite strict DMARC policy; generate forensic report on DKIM failure (fo=d)
        # TODO: is postmaster@ address always there? extra virtual / regular virtual / native domains?
        mkhost.common.dnslog.add_record(('_dmarc.' + ddot, 'TXT', "v=DMARC1; p=reject; sp=reject; pct=100; adkim=s; aspf=s; rf=afrf; fo=d; ri=86400; rua=mailto:postmaster@{}; ruf=mailto:postmaster@{};".format(dom,dom)))
