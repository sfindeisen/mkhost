import mkhost.unix

# Installs and configures PostSRSd.
def install():
    mkhost.unix.install_pkgs(["postsrsd"])

def restart():
    mkhost.unix.restart('postsrsd')
