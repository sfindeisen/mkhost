##############################################################################
# mkhost configuration file.
#
# This file has 2 sections: high-level settings (must be adjusted) and low-
# level settings (nothing to change there, unless you know what you are
# doing).
##############################################################################

import os.path

##############################################################################
# High-level settings
#
# This is your business-level configuration: domains, mailboxes, aliases, mail
# forwarding...
##############################################################################

# A simple, 1-component hostname of the target machine.
# This must correspond to your DNS.
MY_HOST_NAME = "my-host"

# Domain of the target machine, without the hostname.
#
# Thus, the FQDN (fully-qualified domain name) of the target machine will be:
#
#   MY_HOST_NAME . MY_HOST_DOMAIN
#
# This FQDN will be used to obtain Let's Encrypt certificate.
# This must correspond to your DNS.
MY_HOST_DOMAIN = "example.com"

# Whether the target machine should accept mail for the entire MY_HOST_DOMAIN
# domain. This must correspond to your DNS.
MY_HOST_DOMAIN_MX = True

# E-mail address to use with the SSL/TLS certificate.
X509_EMAIL = "{}@{}.{}".format("x509", MY_HOST_NAME, MY_HOST_DOMAIN)

# Mapping between local aliases and local mailboxes (or files or commands), which will
# be written to /etc/aliases . These lookups are recursive. Feel free to get rid of
# anything which is not needed.
#
# postmaster alias is special: it is used in DMARC DNS records output by the script.
# It must stay.
#
# https://unix.stackexchange.com/questions/65013/understanding-etc-aliases-and-what-it-does
#
# http://www.postfix.org/postconf.5.html#alias_database
# http://www.postfix.org/postconf.5.html#alias_maps
LOCAL_ALIASES = {
    "postmaster"      : "root",
    "devnull"         : "/dev/null",                # discard
    "unknown"         : "|\"exit 67\"",             # simulate a "user unknown" error

    "abuse"           : "postmaster",
    "ftp"             : "postmaster",
    "hostmaster"      : "postmaster",
    "hyperkitty"      : "postmaster",               # for Mailman3
    "list-owner"      : "postmaster",
    "maildump"        : "/tmp/maildump.txt",        # delivery to file
    "mailer-daemon"   : "postmaster",
    "nobody"          : "postmaster",
    "noc"             : "postmaster",
    "postorius"       : "postmaster",               # for Mailman3
    "security"        : ["postmaster", "/tmp/maildump.txt"],
    "twice"           : ["root", "x509"],
    "usenet"          : "postmaster",
    "webmaster"       : "postmaster",
    "www"             : "postmaster",
    "www-data"        : "postmaster",
    "x509"            : "postmaster",               # see X509_EMAIL
}

# List of mailboxes (per domain). These will be mapped to Postfix/Dovecot virtual
# users: http://www.postfix.org/VIRTUAL_README.html#virtual_mailbox . Each one
# will need to have a password.
MAILBOXES = {
    "b-server": ["user1", "user2"],
    "c-server": ["eve"]
}

# E-mail address mapping for mail forwarding. A single address can be mapped
# to 1 or more other addresses, possibly on multiple domains.
#
# Note: you don't need to own those domains or mailboxes!
#       You can forward anywhere.
#
# These will be mapped to Postfix virtual aliases, see here:
# http://www.postfix.org/VIRTUAL_README.html#virtual_alias
MAIL_FORWARDING = {
    "alice@a-server"                : "postmaster@b-server",
    # a simple mailing list
    "postmaster@b-server"           : ["alice@a-server", "bob@b-server"],
    "bob@a-server"                  : "bob@b-server",
    "mailtunnel@a-server"           : "endpoint@somewhere-else",
    "bob@b-server"                  : "eve@c-server",
    "postmaster@my-domain.tld"      : "postmaster@b-server"
}

# A hook to configure additional virtual domains with OpenDKIM (if ENABLE_DKIM
# is set) and Let's Encrypt. These domains will not be made known to Postfix
# or Dovecot. This can be useful if, for example, you are also setting up GNU
# Mailman3 with virtual hosts.
#
# Dictionary values are e-mail addresses to be used with Let's Encrypt X.509
# certificates.
EXTRA_VIRTUAL_DOMAINS = {
    "lists.b-server" : "x509@b-server",
    "lists.c-server" : "x509@c-server"
}

# Whether to enable OpenDKIM.
ENABLE_DKIM = True

# Whether to DKIM-sign all outgoing e-mail, including from arbitrary domains.
# This also affects OpenDKIM's SenderHeaders setting.
#
# This can be useful if you send e-mail with arbitrary From: address domains,
# e.g. because you host a mailing list.
DKIM_SIGN_ALL = False

# Enable Sender Rewriting Scheme (SRS):
#
# https://en.wikipedia.org/wiki/Sender_Rewriting_Scheme
#
# This can be useful if your host will act as a forwarder, but it can also
# harm your domain reputation if you forward any spam.
# Check this question:
#
# https://serverfault.com/questions/896791/postfix-forwarding-spf-issues-sender-rewrite
#
# This uses postsrsd daemon and on Debian it is important to realize that the
# domain used for rewriting is equal to Postfix' $mydomain (which is equal to
# MY_HOST_DOMAIN). This is the domain that will receive bounces, for example.
# Therefore if ENABLE_SRS is True, then make sure that MX DNS record for the
# domain (MY_HOST_DOMAIN) points back to this machine and that
# MY_HOST_DOMAIN_MX is also True.
#
# If unsure, leave it to False.
ENABLE_SRS = False

##############################################################################
# Low-level settings
#
# These specify: various filepaths and other system details. It should be safe
# to leave it as is.
##############################################################################

# Path to /etc/aliases file.
#
# https://unix.stackexchange.com/questions/65013/understanding-etc-aliases-and-what-it-does
ETC_ALIASES = '/etc/aliases'

# System user who owns virtual mail files.
# It will be created, if missing.
VIRTUAL_MAIL_USER = 'mkhost-mailv'

# Postfix virtual mailbox base (aka directory where virtual mail is stored).
# This is used by Dovecot, too.
#
# http://www.postfix.org/postconf.5.html#virtual_mailbox_base
VIRTUAL_MAILBOX_BASE = "/var/mail-virtual/"

# Dovecot configuration file
DOVECOT_CONFIG_FILE = "/etc/dovecot/dovecot.conf"

# List of mail protocols to enable in Dovecot. This is mapped directly to
# https://doc.dovecot.org/settings/core/#protocols
DOVECOT_PROTOCOLS = ["imap", "pop3"]

# Whether Dovecot should listen on all available network interfaces (False)
# or just the localhost (True).
# If all your users are local (for example, you always fetch your mail through
# an SSH tunnel), then setting this to True will improve security.
DOVECOT_LOOPBACK_ONLY = False

# Dovecot users database
DOVECOT_USERS_DB = "/etc/dovecot/users.mkhost"

# Dovecot system group name
DOVECOT_GROUP = "dovecot"

# Let's Encrypt home directory
LETSENCRYPT_HOME = "/etc/letsencrypt/"

# Whether Postfix runs in a chroot jail. Note: this setting alone WILL NOT
# setup chroot for Postfix. This is merely to align it correctly with
# other services, most notably Dovecot SASL and OpenDKIM. On Debian 11, chroot
# seems to be the case.
#
# http://www.postfix.org/BASIC_CONFIGURATION_README.html#chroot_setup
POSTFIX_CHROOT = True

# The root of Postfix's chroot jail. Note: it is unlikely that you ever
# need to change this, as it seems to be hardcoded:
# http://www.postfix.org/BASIC_CONFIGURATION_README.html#chroot_setup .
POSTFIX_CHROOT_PATH = "/var/spool/postfix/"

# Path to OpenDKIM's local UNIX domain socket, relative to Postfix chroot.
#
# http://www.postfix.org/postconf.5.html#smtpd_milters
POSTFIX_OPENDKIM_SOCKET = "opendkim/opendkim.sock"

# Postfix system user
POSTFIX_USER = "postfix"

# Postfix virtual alias map file.
#
# http://www.postfix.org/postconf.5.html#virtual_alias_maps
POSTFIX_VIRTUAL_ALIAS_MAP = "/etc/postfix/valias.mkhost"

# Postfix virtual mailbox map file.
#
# http://www.postfix.org/postconf.5.html#virtual_mailbox_maps
POSTFIX_VIRTUAL_MAILBOX_MAP = "/etc/postfix/vmailbox.mkhost"

# Directory where OpenDKIM will store domain keys.
OPENDKIM_KEYS = "/etc/opendkim/mkhost/"
# OpenDKIM's system user/group name
OPENDKIM_USER = "opendkim"
OPENDKIM_GROUP= "opendkim"

# OpenDKIM config file
OPENDKIM_CONF     = "/etc/opendkim.conf"
OPENDKIM_KEYTABLE = "/etc/opendkim-keytable.mkhost"
OPENDKIM_SIGTABLE = "/etc/opendkim-sigtable.mkhost"
# OpenDKIM's local UNIX domain socket (absolute path)
OPENDKIM_SOCKET   = os.path.join(POSTFIX_CHROOT_PATH if POSTFIX_CHROOT else "/", POSTFIX_OPENDKIM_SOCKET)
