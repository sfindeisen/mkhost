import datetime
import logging
import os
import os.path
import re
import shutil
import stat

import mkhost.dns_log

##############################################################################
# Common constants
##############################################################################

re_blank         = re.compile('^\s*$', re.ASCII)
re_comment       = re.compile('^\s*#.*$', re.ASCII)
re_mkhost_header = re.compile('^# Generated by mkhost ([0-9]+)\.([0-9]+) at [0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{6}\+[0-9]{2}:[0-9]{2}$', re.ASCII)

filemode_640     = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP
filemode_644     = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IROTH
filemode_700     = stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR

GENERATE_DKIM_KEYS_ALL     = 'all'
GENERATE_DKIM_KEYS_MISSING = 'missing'
GENERATE_DKIM_KEYS_NONE    = 'none'

##############################################################################
# Common settings
##############################################################################

_version_major      = 0
_version_minor      = 10
_dry_run            = True
_verbose            = False
_non_interactive    = False
_generate_dkim_keys = GENERATE_DKIM_KEYS_MISSING
_run_ts             = datetime.datetime.now(datetime.timezone.utc)     # timestamp of this run

# Returns the version number as a pair (major, minor)
def get_version():
    return (_version_major, _version_minor)

def get_dry_run():
    return _dry_run

def set_dry_run(b):
    global _dry_run
    _dry_run = bool(b)
    logging.debug("[common] _dry_run: {}".format(_dry_run))

def get_non_interactive():
    return _non_interactive

def set_non_interactive(b):
    global _non_interactive
    _non_interactive = bool(b)
    logging.debug("[common] _non_interactive: {}".format(_non_interactive))

def get_verbose():
    return _verbose

def set_verbose(b):
    global _verbose
    _verbose = bool(b)
    logging.debug("[common] _verbose: {}".format(_verbose))

def get_generate_dkim_keys():
    return _generate_dkim_keys

def set_generate_dkim_keys(mode):
    global _generate_dkim_keys
    if mode in [GENERATE_DKIM_KEYS_ALL, GENERATE_DKIM_KEYS_MISSING, GENERATE_DKIM_KEYS_NONE]:
        _generate_dkim_keys = mode
        logging.debug("[common] _generate_dkim_keys: {}".format(_generate_dkim_keys))
    else:
        raise Exception("invalid generate-dkim-keys mode: {}".format(mode))

# Returns the timestamp of this run as a timezone-aware, UTC datetime object.
def get_run_ts():
    return _run_ts

##############################################################################
# Common variables
##############################################################################

dnslog = mkhost.dns_log.DNSLog()

##############################################################################
# Common functions
##############################################################################

# Generates mkhost header string.
def mkhost_header():
    return "# Generated by mkhost {}.{} at {}".format(
        _version_major,
        _version_minor,
        get_run_ts().isoformat())

# Promotes x to a (1-element) list (unless it is a list already).
def tolist(x):
    return (x if isinstance(x,list) else [x])

# Given an e-mail address, returns a pair: (username, domain).
def parse_addr(addr):
    p = addr.partition('@')
    return (p[0],p[2])

# Given an e-mail address (or a collection thereof), returns the domain (or a set thereof).
def addr2dom(addr):
    return addr.partition('@')[2] if isinstance(addr,str) else set(map(lambda x: x.partition('@')[2], addr))

# Given a set of domains and a set of addresses, returns the subset of addresses which belong
# to any of the given domains.
def filter_addr_in_domain(domains, addresses):
    xs = set(filter(lambda x: addr2dom(x) in domains, addresses))
    logging.debug("[common] filter_addr_in_domain: {} + {} => {}".format(domains, addresses, xs))
    return xs

# # Given a string, removes leading and trailing whitespace and combines
# # multiple whitespace characters into a single space.
# # Note: this removes any linebreaks.
# def compress_whitespace(s):
#     return ' '.join(s.split())

# Given an open file and a destination file name, checks if dry-run mode is set.
# If not, flushes the file and copies it to the destination, optionally running
# a command afterwards (mkhost.cmd.execute_cmd).
#
# Group and filemode can be used to setup dest file ownership (chown) and mode
# (chmod) if the file is new.
def flush_copy_execute(f, dest, cmd=None, group=None, filemode=None):
    if not get_dry_run():
        f.flush()
        logging.info("[common] write: {}".format(dest))

        # Now copy the file. If the dest file exists, copy just the contents.
        # Else copy the contents + some metadata (incl. permissions).
        if os.path.isfile(dest):
            shutil.copyfile(f.name, dest)
        else:
            shutil.copy(f.name, dest)
            if group is not None:
                shutil.chown(dest, group=group)
            if filemode is not None:
                os.chmod(dest, filemode)

        # Run the command.
        if cmd:
            mkhost.cmd.execute_cmd(cmd)
