import functools
import logging
import os
import os.path
import re
import secrets
import stat
import string

import mkhost.common
import mkhost.cfg
import mkhost.cfg_parser
import mkhost.file_parser
import mkhost.file_writer
import mkhost.letsencrypt
import mkhost.unix

re_users = re.compile(
    '^([^:]+):\{([-\w]+)\}\$(\w+)\$[^:]*:(\d*):(\d*)::::$', re.ASCII)

pwd_hash_cmd = ["doveadm", "pw", "-s", "SHA512-CRYPT"]

# generate a new password
def gen_pwd():
    alphabet = string.ascii_letters + string.digits
    return ''.join(secrets.choice(alphabet) for i in range(18))

# generate a new user password
def gen_pwd_hash(username):
    # TODO check what password schemes are available: doveadm pw -l
    if mkhost.common.get_non_interactive():
        pwd = gen_pwd()
        logging.info("[dovecot] New password for {}: {}".format(username, pwd))
        return mkhost.cmd.execute_cmd_batch(pwd_hash_cmd, input=(pwd + os.linesep + pwd + os.linesep))[0][0]
        # TODO clear error message if number of output lines != 1
    else:
        logging.info("[dovecot] New password for {}".format(username))
        return mkhost.cmd.execute_cmd_interactive(pwd_hash_cmd)[0][0]
        # TODO clear error message if number of output lines != 1

# Generates Dovecot configuration and writes it to the given configuration file
# (which must be the main Dovecot configuration file).
def write_config():
    with open(mkhost.cfg.DOVECOT_CONFIG_FILE) as f:
        for line in f:
            m = mkhost.common.re_mkhost_header.match(line)
            if m:
                mver = (int(m.group(1)), int(m.group(2)))
                logging.info("[dovecot] Found mkhost {}.{} dovecot configuration header".format(mver[0], mver[1]))
                if (mkhost.common.get_version() <= mver):
                    return

    configuration = """
########################################################################
{}
########################################################################

# Kill all clients when Dovecot master process shuts down.
shutdown_clients = yes

# Disable cleartext authentication unless SSL/TLS is used
# or the connection is local.
disable_plaintext_auth = yes

# Just plain authentication using a password.
# Login mechanism is for Outlook client.
auth_mechanisms = plain login

# Allow full filesystem access to clients?
mail_full_filesystem_access = no

# Verbose logging so that we know what is going on.
auth_verbose      = yes
verbose_ssl       = no
verbose_proctitle = yes

protocols    = {}
""".format(mkhost.common.mkhost_header(),
           " ".join(mkhost.cfg.DOVECOT_PROTOCOLS))

    # Listen on the loopback address only
    if mkhost.cfg.DOVECOT_LOOPBACK_ONLY:
        logging.info("[dovecot] Dovecot will listen on IPv4 and IPv6 loopback addresses only")
        configuration += """
listen = 127.0.0.1, ::1
"""
    else:
        logging.info("[dovecot] Dovecot will listen on all available interfaces")

    configuration += """
########################################################################
# Mail layout
########################################################################

mail_location = maildir:~/Maildir/

namespace inbox {
  type  = private
  inbox = yes

  mailbox Drafts {
    auto        = create
    special_use = \Drafts
  }
  mailbox Junk {
    auto        = create
    special_use = \Junk
  }
  mailbox Sent {
    auto        = subscribe
    special_use = \Sent
  }
  mailbox Trash {
    auto        = create
    special_use = \Trash
  }
  mailbox virtual/All {
    auto        = no
    special_use = \All
  }
}
"""

    configuration += """
########################################################################
# Authentication for system users
########################################################################

passdb {{
  driver          = pam
  args            = dovecot
  override_fields = allow_nets=127.0.0.1/32,::1/128
}}

userdb {{
  driver          = passwd

  # skip if an earlier userdb already found the user
  skip = found

  # If multiple userdbs are required (results are merged), it is important to set
  # result_internalfail=return-fail to them, otherwise the userdb lookup could
  # still succeed but not all the intended extra fields are set.
  result_internalfail = return-fail
}}

########################################################################
# Authentication for virtual users (passwd-file).
#
# Virtual user credentials are in {} .
########################################################################

passdb {{
  driver = passwd-file
  args   = scheme=SHA512-CRYPT username_format=%u {}
}}

userdb {{
  driver = passwd-file
  args   = username_format=%u {}

  # Default fields that can be overridden by passwd-file
  default_fields  = uid={} gid={} home={} mail=maildir:{} quota_rule=*:storage=1M

  # Override fields from passwd-file
  override_fields = uid={} gid={} home={} mail=maildir:{}

  # skip if an earlier userdb already found the user
  skip = found

  # If multiple userdbs are required (results are merged), it is important to set
  # result_internalfail=return-fail to them, otherwise the userdb lookup could
  # still succeed but not all the intended extra fields are set.
  result_internalfail = return-fail
}}
""".format(mkhost.cfg.DOVECOT_USERS_DB,
           mkhost.cfg.DOVECOT_USERS_DB,
           mkhost.cfg.DOVECOT_USERS_DB,
           mkhost.cfg.VIRTUAL_MAIL_USER,
           mkhost.cfg.VIRTUAL_MAIL_USER,
           os.path.join(mkhost.cfg.VIRTUAL_MAILBOX_BASE, '%d/%n/'),
           os.path.join(mkhost.cfg.VIRTUAL_MAILBOX_BASE, '%d/%n/mail/'),
           mkhost.cfg.VIRTUAL_MAIL_USER,
           mkhost.cfg.VIRTUAL_MAIL_USER,
           os.path.join(mkhost.cfg.VIRTUAL_MAILBOX_BASE, '%d/%n/'),
           os.path.join(mkhost.cfg.VIRTUAL_MAILBOX_BASE, '%d/%n/mail/'))

    # TODO: what if Postfix doesn't run in chroot?...
    configuration += """
########################################################################
# User authentication service for Postfix (SASL)
########################################################################

service auth {{
  unix_listener {} {{
    group = postfix
    mode  = 0660
    user  = postfix
  }}
}}
""".format(os.path.join(mkhost.cfg.POSTFIX_CHROOT_PATH, "private/auth"))

    configuration += """
########################################################################
# SSL settings
########################################################################

ssl                       = required
ssl_prefer_server_ciphers = yes

ssl_cert = <{}
ssl_key  = <{}
""".format(mkhost.letsencrypt.fullchain_path(),
           mkhost.letsencrypt.key_path())

    # overwrite the configuration file
    if not mkhost.common.get_dry_run():
        logging.info("[dovecot] writing configuration to {}".format(mkhost.cfg.DOVECOT_CONFIG_FILE))
        with open(mkhost.cfg.DOVECOT_CONFIG_FILE, "a") as f:
            print(configuration, file=f)

    logging.debug(configuration)

# Given a match object which is assumed to be a match of re_users regex in
# DOVECOT_USERS_DB file, updates the given mailbox set (virtual_mailboxes)
# accordingly.
#
# This is to be used as callback function with mkhost.file_parser.parse_config_file.
def _parse_users_db_line_match(virtual_mailboxes, m):
    username = m.group(1)

    if username in virtual_mailboxes:
        logging.info("[dovecot] user already exists: {}".format(username))
        virtual_mailboxes.remove(username)
        return True
    else:
        logging.info("[dovecot] delete user: {}".format(username))
        return False

# Generates and writes out user database file (mkhost.cfg.DOVECOT_USERS_DB).
def write_users_db():
    virtual_mailboxes = mkhost.cfg_parser.get_virtual_mailboxes()

    old_lines = mkhost.file_parser.parse_config_file(
        mkhost.cfg.DOVECOT_USERS_DB,
        re_users,
        functools.partial(_parse_users_db_line_match, virtual_mailboxes))

    # TODO: file permissions?...
    # create new user db file
    with mkhost.file_writer.create_tmp_text_file(old_lines) as f:
        if virtual_mailboxes:
            mkhost.file_writer.write_mkhost_header(f)
            for x in virtual_mailboxes:
                logging.info("[dovecot] create user: {}".format(x))
                print("{}:{}::::::".format(x,gen_pwd_hash(x)), file=f)

        # overwrite old user db file
        mkhost.common.flush_copy_execute(f, mkhost.cfg.DOVECOT_USERS_DB, group=mkhost.cfg.DOVECOT_GROUP, filemode=mkhost.common.filemode_640)

# Installs and configures Dovecot.
def install():
    mkhost.unix.install_pkgs(["dovecot-imapd"])

    write_config()
    write_users_db()

def restart():
    mkhost.unix.restart('dovecot')
