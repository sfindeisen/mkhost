import logging
import os.path

import mkhost.cfg
import mkhost.common

# Returns the fully qualified domain name of this host (MY_HOST_NAME . MY_HOST_DOMAIN).
# This used to be MY_HOST_FULLNAME configuration setting.
def get_host_fullname():
    return "{}.{}".format(mkhost.cfg.MY_HOST_NAME, mkhost.cfg.MY_HOST_DOMAIN)

# Given MAIL_FORWARDING (in the config file), compute the outgoing addresses (those mapped to, but
# not mapped from). Can include mailboxes and 3rd party addresses.
def get_fwd_dst_addresses():
    vals2     = map(mkhost.common.tolist, mkhost.cfg.MAIL_FORWARDING.values())
    vals      = set(x for ys in vals2 for x in ys)
    keys      = mkhost.cfg.MAIL_FORWARDING.keys()
    outgoing  = vals.difference(keys)
    logging.debug("[cfg_parser] get_fwd_dst_addresses: {}".format(outgoing))
    return outgoing

# Given MAILBOXES and MAIL_FORWARDING (in the config file), compute the
# virtual alias domains (mailbox-less domains used for mail forwarding).
#
# http://www.postfix.org/postconf.5.html#virtual_alias_domains
def get_virtual_alias_domains() -> set:
    keydoms = mkhost.common.addr2dom(mkhost.cfg.MAIL_FORWARDING.keys())
    aliased = keydoms.difference(mkhost.cfg.MAILBOXES.keys())
    logging.debug("[cfg_parser] get_virtual_alias_domains: {}".format(aliased))
    return aliased

# Given MAILBOXES (in the config file), compute the virtual mailbox
# domains (those which can contain mailboxes).
#
# http://www.postfix.org/postconf.5.html#virtual_mailbox_domains
def get_virtual_mailbox_domains() -> set:
    return set(mkhost.cfg.MAILBOXES.keys())

def get_extra_virtual_domains() -> set:
    return set(mkhost.cfg.EXTRA_VIRTUAL_DOMAINS.keys())

# Returns the set of all virtual domains (virtual mailbox domains + virtual
# alias domains) but by default without EXTRA_VIRTUAL_DOMAINS.
def get_virtual_domains(include_extra=False) -> set:
    return (get_virtual_mailbox_domains()
            | get_virtual_alias_domains()
            | (get_extra_virtual_domains() if include_extra else set()))

# Returns the set of all virtual and non-virtual domains, optionally
# with EXTRA_VIRTUAL_DOMAINS included.
def get_all_domains(include_extra=True) -> set:
    return (get_virtual_domains(include_extra=include_extra)
            | {get_host_fullname()})

# Given MAILBOXES and FORWARDING (in the config file), compute the
# virtual mailbox set (hosted virtual mailboxes).
def get_virtual_mailboxes():
    mailboxes = set("{}@{}".format(x,d) for d, xs in mkhost.cfg.MAILBOXES.items() for x in xs)
    logging.debug("[cfg_parser] get_virtual_mailboxes: {}".format(mailboxes))
    return mailboxes

def show_config():
    ver = mkhost.common.get_version()
    logging.info("{: <30}: {}.{}".format("mkhost version",             ver[0], ver[1]))
    logging.info("{: <30}: {}".format("MY_HOST_NAME",                  mkhost.cfg.MY_HOST_NAME))
    logging.info("{: <30}: {}".format("MY_HOST_DOMAIN",                mkhost.cfg.MY_HOST_DOMAIN))
    logging.info("{: <30}: {}".format("MY_HOST_DOMAIN_MX",             mkhost.cfg.MY_HOST_DOMAIN_MX))
    logging.info("{: <30}: {}".format("X509_EMAIL",                    mkhost.cfg.X509_EMAIL))
    logging.info("{: <30}: {}".format("MAILBOXES",                     mkhost.cfg.MAILBOXES))
    logging.info("{: <30}: {}".format("MAIL_FORWARDING",               mkhost.cfg.MAIL_FORWARDING))
    logging.info("{: <30}: {}".format("EXTRA_VIRTUAL_DOMAINS",         mkhost.cfg.EXTRA_VIRTUAL_DOMAINS))
    logging.info("{: <30}: {}".format("ENABLE_DKIM",                   mkhost.cfg.ENABLE_DKIM))
    logging.info("{: <30}: {}".format("ENABLE_SRS",                    mkhost.cfg.ENABLE_SRS))
    logging.info("{: <30}: {}".format("VIRTUAL_MAIL_USER",             mkhost.cfg.VIRTUAL_MAIL_USER))
    logging.info("{: <30}: {}".format("VIRTUAL_MAILBOX_BASE",          mkhost.cfg.VIRTUAL_MAILBOX_BASE))
    logging.info("{: <30}: {}".format("DOVECOT_CONFIG_FILE",           mkhost.cfg.DOVECOT_CONFIG_FILE))
    logging.info("{: <30}: {}".format("DOVECOT_PROTOCOLS",             mkhost.cfg.DOVECOT_PROTOCOLS))
    logging.info("{: <30}: {}".format("DOVECOT_LOOPBACK_ONLY",         mkhost.cfg.DOVECOT_LOOPBACK_ONLY))
    logging.info("{: <30}: {}".format("DOVECOT_USERS_DB",              mkhost.cfg.DOVECOT_USERS_DB))
    logging.info("{: <30}: {}".format("LETSENCRYPT_HOME",              mkhost.cfg.LETSENCRYPT_HOME))
    logging.info("{: <30}: {}".format("POSTFIX_CHROOT",                mkhost.cfg.POSTFIX_CHROOT))
    logging.info("{: <30}: {}".format("POSTFIX_CHROOT_PATH",           mkhost.cfg.POSTFIX_CHROOT_PATH))
    logging.info("{: <30}: {}".format("POSTFIX_OPENDKIM_SOCKET",       mkhost.cfg.POSTFIX_OPENDKIM_SOCKET))
    logging.info("{: <30}: {}".format("POSTFIX_USER",                  mkhost.cfg.POSTFIX_USER))
    logging.info("{: <30}: {}".format("POSTFIX_VIRTUAL_ALIAS_MAP",     mkhost.cfg.POSTFIX_VIRTUAL_ALIAS_MAP))
    logging.info("{: <30}: {}".format("POSTFIX_VIRTUAL_MAILBOX_MAP",   mkhost.cfg.POSTFIX_VIRTUAL_MAILBOX_MAP))
    logging.info("{: <30}: {}".format("OPENDKIM_KEYS",                 mkhost.cfg.OPENDKIM_KEYS))
    logging.info("{: <30}: {}".format("OPENDKIM_USER",                 mkhost.cfg.OPENDKIM_USER))
    logging.info("{: <30}: {}".format("OPENDKIM_GROUP",                mkhost.cfg.OPENDKIM_GROUP))
    logging.info("{: <30}: {}".format("OPENDKIM_CONF",                 mkhost.cfg.OPENDKIM_CONF))
    logging.info("{: <30}: {}".format("OPENDKIM_KEYTABLE",             mkhost.cfg.OPENDKIM_KEYTABLE))
    logging.info("{: <30}: {}".format("OPENDKIM_SIGTABLE",             mkhost.cfg.OPENDKIM_SIGTABLE))
    logging.info("{: <30}: {}".format("OPENDKIM_SOCKET",               mkhost.cfg.OPENDKIM_SOCKET))
    logging.info("{: <30}: {}".format("get_all_domains()",             get_all_domains()))
    logging.info("{: <30}: {}".format("get_extra_virtual_domains()",   get_extra_virtual_domains()))
    logging.info("{: <30}: {}".format("get_fwd_dst_addresses()",       get_fwd_dst_addresses()))
    logging.info("{: <30}: {}".format("get_host_fullname()",           get_host_fullname()))
    logging.info("{: <30}: {}".format("get_virtual_alias_domains()",   get_virtual_alias_domains()))
    logging.info("{: <30}: {}".format("get_virtual_mailbox_domains()", get_virtual_mailbox_domains()))
    logging.info("{: <30}: {}".format("get_virtual_domains()",         get_virtual_domains()))
    logging.info("{: <30}: {}".format("get_virtual_mailboxes()",       get_virtual_mailboxes()))

def show_version():
    ver = mkhost.common.get_version()
    print("mkhost {}.{}".format(ver[0], ver[1]))

def validate():
    # check for domains ending with a dot (.)
    if mkhost.cfg.MY_HOST_DOMAIN.endswith("."):
        raise Exception("MY_HOST_DOMAIN must not end with a dot (.)")
    for d in get_all_domains(include_extra=True):
        if d.endswith("."):
            raise Exception("Domain ends with a dot (.): {}".format(d))

    # check is hostname and host domain are virtual too
    if get_host_fullname() in get_virtual_domains(include_extra=True):
        raise Exception("Host name (MY_HOST_NAME.MY_HOST_DOMAIN) cannot be a virtual domain!")
    if mkhost.cfg.MY_HOST_DOMAIN in get_virtual_domains(include_extra=True):
        raise Exception("Host domain (MY_HOST_DOMAIN) cannot be a virtual domain!")

    # check if all virtual domain mailboxes declared on the right hand side of MAIL_FORWARDING
    # are declared in MAILBOXES
    outhosted = mkhost.common.filter_addr_in_domain(mkhost.cfg.MAILBOXES.keys(), get_fwd_dst_addresses())
    outhosted = outhosted.difference(get_virtual_mailboxes())
    if outhosted:
        raise Exception("Extra addresses on the right hand side in MAIL_FORWARDING: {}. They belong to MAILBOXES domains. Did you forget to declare them in MAILBOXES?".format(outhosted))

    # check if extra virtual domains overlap with regular virtual domains
    intersect = get_virtual_domains(include_extra=False) & get_extra_virtual_domains()
    if intersect:
        raise Exception("The following domains are both in EXTRA_VIRTUAL_DOMAINS and in MAILBOXES: {}".format(intersect))

    if mkhost.cfg.ENABLE_SRS and (not mkhost.cfg.MY_HOST_DOMAIN_MX):
        raise Exception("ENABLE_SRS is True, but MY_HOST_DOMAIN_MX is False; this will result in broken bounce processing!")
    if not os.path.isabs(mkhost.cfg.ETC_ALIASES):
        raise Exception("ETC_ALIASES must be an absolute path, not a relative one.")
    if os.path.isabs(mkhost.cfg.POSTFIX_OPENDKIM_SOCKET):
        raise Exception("POSTFIX_OPENDKIM_SOCKET must be a relative path, not an absolute one.")
    if "postmaster" not in mkhost.cfg.LOCAL_ALIASES:
        raise Exception("LOCAL_ALIASES must contain postmaster key")
