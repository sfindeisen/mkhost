import copy
import functools
import logging
import os
import os.path
import pathlib
import re
import shutil
import tempfile

import mkhost.cfg
import mkhost.cfg_parser
import mkhost.common
import mkhost.file_parser
import mkhost.file_writer
import mkhost.unix

re_key_value = re.compile(
    '^([-\w]+)\s+(\S+)\s*$', re.ASCII)
# keytable line 1st field (just the domain key)
re_keytable = re.compile(
    '^((?:[a-zA-Z0-9_-]+\.)*(?:[a-zA-Z0-9_-]+)\.?)\s+', re.ASCII)
# opendkim-genkey public key file: domain name
re_pubkey_file_dom = re.compile(
    '^(\S+)\s+IN\s+TXT\s+')
# opendkim-genkey public key file: DNS record payload
re_pubkey_file_chunk = re.compile(
    '"([^"]*)"', re.ASCII)

# OpenDKIM config.
#
# The following represents what we believe to be a sane choice. If you need
# something else, then we perhaps need a config file setting for that.
OPENDKIM_CONFIG = {
    "AllowSHA1Only"              : False,
    "AuthservIDWithJobID"        : False,

    # relaxed canonicalization of the header/body
    #
    # https://datatracker.ietf.org/doc/html/rfc6376/#section-3.4
    # https://stackoverflow.com/questions/8660307/why-would-i-choose-simple-over-relaxed-canonicalization-for-dkim
    "Canonicalization"           : "relaxed/relaxed",

    # Gives  the  location of a file mapping key names to signing keys.
    "KeyTable"                   : "file:" + mkhost.cfg.OPENDKIM_KEYTABLE,
    "LogResults"                 : True,
    "LogWhy"                     : True,
    "MaximumSignaturesToVerify"  : 3,
    "Mode"                       : "sv",
    # Sign using multiple signatures (when applicable).
    "MultipleSignatures"         : True,
    "On-BadSignature"            : "quarantine",
    "Quarantine"                 : True,
    "RequireSafeKeys"            : True,
    "SenderHeaders"              : 'List-Post,Sender,From' if mkhost.cfg.DKIM_SIGN_ALL else 'From',
    "SignatureTTL"               : 14 * 24 * 60 * 60,       # 2 weeks

    # Defines  a  table  used to select one or more signatures to apply to a
    # message based on the address found in the From: header field.
    "SigningTable"               : "file:" + mkhost.cfg.OPENDKIM_SIGTABLE,

    "SendReports"                : False,
    "Socket"                     : "local:" + mkhost.cfg.OPENDKIM_SOCKET,
    "SyslogSuccess"              : True,
}

def gen_selector():
    return mkhost.common.get_run_ts().strftime("%Y%m%d%H%M%S")

# Generates and writes out OpenDKIM signing table file (mkhost.cfg.OPENDKIM_SIGTABLE).
def write_sigtable():
    domains  = mkhost.cfg_parser.get_all_domains()

    # create new config file
    with mkhost.file_writer.create_tmp_text_file() as f:
        for d in domains:
            logging.info("[opendkim] sigtable: {}".format(d))
            print("{:<40} {}".format(d,d), file=f)

        # wildcard domain sign
        if mkhost.cfg.DKIM_SIGN_ALL:
            print("{:<40} {}".format('*', mkhost.cfg_parser.get_host_fullname()), file=f)

        # overwrite the old config file
        logging.info("[opendkim] write sigtable to {}".format(mkhost.cfg.OPENDKIM_SIGTABLE))
        mkhost.common.flush_copy_execute(f, mkhost.cfg.OPENDKIM_SIGTABLE, filemode=mkhost.common.filemode_644)

# Given a match object which is assumed to be a match of re_keytable regex in
# OPENDKIM_KEYTABLE file, updates the given domain set accordingly.
#
# This is to be used as callback function with mkhost.file_parser.parse_config_file.
def _parse_keytable_line_match(domains, m):
    dom = m.group(1)

    if dom in domains:
        logging.debug("[opendkim] keytable domain already exists: {}".format(dom))
        domains.discard(dom)
        return True
    else:
        logging.info("[opendkim] keytable drop domain: {}".format(dom))
        return False

# Rewrites the current OpenDKIM keytable file (mkhost.cfg.OPENDKIM_KEYTABLE), keeping
# existing entries if the domain is on the configured domain list (and deleting them
# otherwise). Then generates new entries for the new domains.
#
# Returns the new domains.
def rewrite_keytable():
    selector = gen_selector()
    domains  = mkhost.cfg_parser.get_all_domains()

    old_lines = mkhost.file_parser.parse_config_file(
        mkhost.cfg.OPENDKIM_KEYTABLE,
        re_keytable,
        functools.partial(_parse_keytable_line_match, domains))

    with mkhost.file_writer.create_tmp_text_file(old_lines) as f:
        for d in domains:
            logging.info("[opendkim] keytable: {}".format(d))
            pk_path  = os.path.join(mkhost.cfg.OPENDKIM_KEYS, d, "{}.private".format(selector))     # private key file
            key_name = d                                                                            # key name is just the domain name
            print("{:<40} {}:{}:{}".format(key_name, d, selector, pk_path), file=f)

        # overwrite the old config file
        logging.info("[opendkim] write keytable to {}".format(mkhost.cfg.OPENDKIM_KEYTABLE))
        mkhost.common.flush_copy_execute(f, mkhost.cfg.OPENDKIM_KEYTABLE, filemode=mkhost.common.filemode_644)

        # return the new domains
        return domains

# Generates and writes out OpenDKIM keytable file (mkhost.cfg.OPENDKIM_KEYTABLE).
def write_keytable():
    selector = gen_selector()
    domains  = mkhost.cfg_parser.get_all_domains()

    # create new config file
    with mkhost.file_writer.create_tmp_text_file() as f:
        for d in domains:
            logging.info("[opendkim] keytable: {}".format(d))
            pk_path  = os.path.join(mkhost.cfg.OPENDKIM_KEYS, d, "{}.private".format(selector))     # private key file
            key_name = d                                                                            # key name is just the domain name
            print("{:<40} {}:{}:{}".format(key_name, d, selector, pk_path), file=f)

        # overwrite the old config file
        logging.info("[opendkim] write keytable to {}".format(mkhost.cfg.OPENDKIM_KEYTABLE))
        mkhost.common.flush_copy_execute(f, mkhost.cfg.OPENDKIM_KEYTABLE, filemode=mkhost.common.filemode_644)

# Given a match object which is assumed to be a match of re_key_value regex in
# OPENDKIM_CONF file, updates the given OpenDKIM config (new_cfg) accordingly.
#
# This is to be used as callback function with mkhost.file_parser.parse_config_file.
def _parse_conf_line_match(new_cfg, m):
    key = m.group(1)
    val = m.group(2)

    if (key not in OPENDKIM_CONFIG) or (str(OPENDKIM_CONFIG[key]) == val):
        logging.debug("[opendkim] keep: {} => {}".format(key,val))
        new_cfg.pop(key,None)
        return True
    else:
        logging.debug("[opendkim] drop: {} => {}".format(key,val))
        return False

# Generates and writes out OpenDKIM config file (mkhost.cfg.OPENDKIM_CONF).
def write_conf():
    new_cfg   = copy.deepcopy(OPENDKIM_CONFIG)
    old_lines = mkhost.file_parser.parse_config_file(
        mkhost.cfg.OPENDKIM_CONF,
        re_key_value,
        functools.partial(_parse_conf_line_match, new_cfg))

    # create new config file
    with mkhost.file_writer.create_tmp_text_file(old_lines) as f:
        if new_cfg:
            mkhost.file_writer.write_mkhost_header(f)
            for x,y in new_cfg.items():
                logging.info("[opendkim] cfg: {} => {}".format(x,y))
                print("{:<30} {}".format(x,y), file=f)

        # overwrite the old config file
        logging.info("[opendkim] write config to {}".format(mkhost.cfg.OPENDKIM_CONF))
        mkhost.common.flush_copy_execute(f, mkhost.cfg.OPENDKIM_CONF, filemode=mkhost.common.filemode_644)

# Parses OpenDKIM's pubkey file contents into a DNS record 3-tuple
def parse_pubkey_file(pubkey_file_contents):
    logging.debug("[opendkim] parse_pubkey_file: {}".format(pubkey_file_contents))
    m = re_pubkey_file_dom.match(pubkey_file_contents)
    if m:
        dom     = m.group(1)
        payload = ''.join(re.findall(re_pubkey_file_chunk, pubkey_file_contents[m.end(0):]))
        logging.debug("[opendkim] parse_pubkey_file: {} TXT {}".format(dom, payload))
        return (dom, 'TXT', payload)
    else:
        raise Exception("[opendkim] Error parsing opendkim-genkey's public key output file: {}".format(pubkey_file_contents))

def set_pubkey_file_perm(filepath):
    shutil.chown(filepath, user=mkhost.cfg.OPENDKIM_USER, group=mkhost.cfg.OPENDKIM_GROUP)
    os.chmod(filepath, mkhost.common.filemode_644)

def set_prvkey_file_perm(filepath):
    shutil.chown(filepath, user=mkhost.cfg.OPENDKIM_USER, group="root")
    os.chmod(filepath, mkhost.common.filemode_640)

# Given a domain name, generates a selector, a public-private key pair
# and writes them to a file.
def genkey(domain):
    selector = gen_selector()
    logging.debug("[opendkim] selector: {}; domain: {}".format(selector, domain))

    if not mkhost.common.get_dry_run():
        domain_dir = os.path.join(mkhost.cfg.OPENDKIM_KEYS, domain)
        os.makedirs(domain_dir, mode=0o755, exist_ok=True)

    try:
        tempdir = tempfile.mkdtemp(prefix="mkhost-")
        logging.debug("[opendkim] tempdir: {}".format(tempdir))
        mkhost.cmd.execute_cmd([
            "opendkim-genkey", "-a", "-r", "-d", domain, "-s", selector, "-D", tempdir])

        if not mkhost.common.get_dry_run():
            # write the public key file
            dns_rec_file = "{}.txt".format(selector)
            dns_rec_path = os.path.join(tempdir, dns_rec_file)
            rec = parse_pubkey_file(pathlib.Path(dns_rec_path).read_text())
            mkhost.common.dnslog.add_record(rec)
            set_pubkey_file_perm(dns_rec_path)

            # move the public key file
            shutil.move(dns_rec_path, domain_dir)
            set_pubkey_file_perm(os.path.join(domain_dir, dns_rec_file))

            # write the private key file
            pk_file = "{}.private".format(selector)
            pk_path = os.path.join(tempdir, pk_file)
            set_prvkey_file_perm(pk_path)

            # move the private key file
            shutil.move(os.path.join(tempdir, pk_file), domain_dir)
            set_prvkey_file_perm(os.path.join(domain_dir, pk_file))

        shutil.rmtree(tempdir,ignore_errors=True)
    except shutil.Error as e:
        shutil.rmtree(tempdir, ignore_errors=True)
        logging.warning("[opendkim] Error installing new OpenDKIM keys for {}, skipping: {}".format(domain, e))
    except Exception as e:
        shutil.rmtree(tempdir, ignore_errors=True)
        raise

# Installs and configures OpenDKIM.
def install():
    mkhost.unix.install_pkgs(["opendkim", "opendkim-tools"])

    if mkhost.common.GENERATE_DKIM_KEYS_ALL == mkhost.common.get_generate_dkim_keys():
        domains = mkhost.cfg_parser.get_all_domains()
        logging.info("[opendkim] generate DKIM keys for all domains: {}".format(domains))
        for d in domains:
            genkey(d)

        write_sigtable()
        write_keytable()
    elif mkhost.common.GENERATE_DKIM_KEYS_MISSING == mkhost.common.get_generate_dkim_keys():
        write_sigtable()
        domains = rewrite_keytable()

        for d in domains:
            genkey(d)

    # rewrite the config file
    write_conf()

def restart():
    mkhost.unix.restart('opendkim')
