import logging
import re

import mkhost.common

# Parses the given configuration text file line by line. If the line is a comment
# (mkhost.common.re_comment) or blank (mkhost.common.re_blank), it is yielded.
# If otherwise the line matches the regex, the match object is yielded.
# Else an exception is thrown.
def _parse_config_file_lines(filename, regex):
    try:
        with open(filename) as f:
            for line in map(lambda x: x.rstrip(), f):
                if mkhost.common.re_comment.match(line):
                    yield line
                elif mkhost.common.re_blank.match(line):
                    yield line
                else:
                    m = regex.match(line)
                    if m:
                        yield m
                    else:
                        raise Exception("[file_parser] {}: invalid line: {}".format(filename, line))
    except FileNotFoundError:
        logging.warning("[file_parser] file does not exist: {}".format(filename))

# Parses the given configuration text file line by line, calling the given callback
# function on each regex match. Meanwhile, existing lines are retained in a list
# and later returned, but only those, for which callback doesn't return False.
def parse_config_file(filename, regex, callback):
    old_lines = []

    for x in _parse_config_file_lines(filename, regex):
        if isinstance(x, re.Match):
            if callback(x):
                old_lines.append(x.string)
        elif isinstance(x, str):
            old_lines.append(x)
        else:
            raise Exception("Not implemented: {}".format(x))

    return old_lines
